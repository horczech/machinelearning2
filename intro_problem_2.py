import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib
from mpl_toolkits.mplot3d import Axes3D


print(matplotlib.__version__)

csvname = '/Users/horczech/Desktop/Dresden VOL.1/Praktikum_ML2/intro_exercise/Data_3D_2classes.csv'

print('Reading "' + csvname + '":')
dat = np.loadtxt(csvname, delimiter=';')

# print(dat)

df = pd.DataFrame(dat)
print(df.describe())

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')


for i, d in enumerate(dat):
    if d[3] > 0:
        ax.scatter(d[0], d[1], d[2], color='#0000FF', marker='o')
    else:
        ax.scatter(d[0], d[1], d[2], color='#FF8000', marker='o')

plt.title('Distribution (two classes)')
ax.set_xlabel('Random points')
ax.set_ylabel('Class 1 = Blue, Class -1 = Orange')
ax.set_zlabel('Z Label')
plt.show()