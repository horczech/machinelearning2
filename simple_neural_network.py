import numpy as np
import tensorflow as tf

n_features = 10
n_dense_neurons = 3

# Placeholder for x
x = tf.placeholder(tf.float32,(None,n_features))

# Variables for w and b
b = tf.Variable(tf.zeros([n_dense_neurons]))
W = tf.Variable(tf.random_normal([n_features,n_dense_neurons]))

# operations
xW = tf.matmul(x,W)
z = tf.add(xW,b)
a = tf.sigmoid(z)

# variable initializer
init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)

    layer_out = sess.run(a, feed_dict={x: np.random.random([1, n_features])})

print(layer_out)
