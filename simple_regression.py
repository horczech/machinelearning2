import tensorflow as tf
import numpy as np
from matplotlib import pyplot as plt

# create noisy linear dataset
x_data = np.linspace(0, 10, 10) + np.random.uniform(-1.5, 1.5, 10)
y_data = np.linspace(0, 10, 10) + np.random.uniform(-1.5, 1.5, 10)

# plot the data
plt.figure(1)
plt.plot(x_data, y_data, '*')

# initialize weight and bias with random numbers
m_var, b_var = np.random.rand(2)

m = tf.Variable(m_var)
b = tf.Variable(b_var)

# create cost function
error = 0
for x, y in zip(x_data, y_data):
    # Our predicted value
    y_hat = m * x + b

    # The cost we want to minimize (we'll need to use an optimization function for the minimization!)
    error += (y - y_hat) ** 2

# create optimizer
optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
train = optimizer.minimize(error)

# initialize variables
init = tf.global_variables_initializer()

# create session
with tf.Session() as sess:
    sess.run(init)

    epochs = 100

    for i in range(epochs):
        sess.run(train)

    # Fetch Back Results
    final_slope, final_intercept = sess.run([m, b])

# evaluate results
plt.figure(2)
x_test = np.linspace(-1, 11, 10)
y_pred_plot = final_slope * x_test + final_intercept

plt.plot(x_test, y_pred_plot, 'r')

plt.plot(x_data, y_data, '*')


plt.show()