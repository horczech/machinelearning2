import pandas as pd
import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt
import matplotlib
from sklearn.model_selection import train_test_split
import logging

tf.logging.set_verbosity(tf.logging.INFO)



path = r'/Users/horczech/Desktop/coursea/Tensorflow_and_machineLearning/Tensorflow-Bootcamp-master/02-TensorFlow-Basics/pima-indians-diabetes.csv'
data = pd.read_csv(path)


cols_to_normalise = ['Number_pregnant',
                     'Glucose_concentration',
                     'Blood_pressure',
                     'Triceps',
                     'Insulin',
                     'BMI',
                     'Pedigree']

data[cols_to_normalise] = data[cols_to_normalise].apply(lambda x: (x-x.min())/(x.max()-x.min()))


# continuous feature values
num_preg = tf.feature_column.numeric_column('Number_pregnant')
plasma_gluc = tf.feature_column.numeric_column('Glucose_concentration')
dias_press = tf.feature_column.numeric_column('Blood_pressure')
tricep = tf.feature_column.numeric_column('Triceps')
insulin = tf.feature_column.numeric_column('Insulin')
bmi = tf.feature_column.numeric_column('BMI')
diabetes_pedigree = tf.feature_column.numeric_column('Pedigree')
age = tf.feature_column.numeric_column('Age')

# categorical features
assigned_group = tf.feature_column.categorical_column_with_vocabulary_list('Group', ['A', 'B', 'C', 'D'])

plt.figure('Age hist')
data['Age'].hist(bins=20)

age_buckets = tf.feature_column.bucketized_column(age, boundaries=[20,30,40,50,60,70,80])

# put all feature collumns together
feat_cols = [num_preg,plasma_gluc,dias_press ,tricep ,insulin,bmi,diabetes_pedigree ,assigned_group, age_buckets]

# do the test split

x_data = data.drop('Class', axis=1)
labels = data['Class']

X_train, X_test, y_train, y_test = train_test_split( x_data, labels, test_size=0.3, random_state=101)


# CREATE MODEL

input_func = tf.estimator.inputs.pandas_input_fn(x=x_data,y=labels, batch_size=10, num_epochs=1000, shuffle=True)
model = tf.estimator.LinearClassifier(feature_columns=feat_cols, n_classes=2)
model.train(input_fn=input_func,steps=1000)

# evaluate model
eval_input_func = tf.estimator.inputs.pandas_input_fn(x=X_test, y=y_test,batch_size=10, num_epochs=1, shuffle=False)
results = model.evaluate(input_fn=eval_input_func)



print(results)

pred_input_func = tf.estimator.inputs.pandas_input_fn(x=X_test,batch_size=10,num_epochs=1, shuffle=False)
predistions = model.predict(input_fn=pred_input_func)

my_pred = list(predistions)

print(my_pred)

